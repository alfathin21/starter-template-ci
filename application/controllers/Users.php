<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function index()
	{
                $data['title'] = "Users Access Control";
                $this->load->view('pages/head',$data);
                $this->load->view('pages/side');
                $this->load->view('pages/users');
                $this->load->view('pages/foot');
	}
}
