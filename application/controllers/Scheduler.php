<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scheduler extends CI_Controller {

	public function index()
	{
                $data['title'] = "Scheduler Control";
                $this->load->view('pages/head',$data);
                $this->load->view('pages/side');
                $this->load->view('pages/sched');
                $this->load->view('pages/foot');
	}
}
