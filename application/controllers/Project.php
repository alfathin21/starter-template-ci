<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function index()
	{
                $data['title'] = "Project";
                $this->load->view('pages/head',$data);
                $this->load->view('pages/side');
                $this->load->view('pages/project');
                $this->load->view('pages/foot');
	}
}
