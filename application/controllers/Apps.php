<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apps extends CI_Controller
{

    public function index()
    {
        $data['title'] = "Apps";
        $this->load->view('layout/head', $data);
        $this->load->view('layout/nav');
        $this->load->view('pages/apps');
        $this->load->view('layout/foot');
    }
}
