<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Dashboard Starter</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>assets/control/img/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/owl.carousel.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/owl.theme.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/owl.transitions.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/animate.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/normalize.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/meanmenu.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/main.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/educate-custon-icon.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/morrisjs/morris.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/scrollbar/jquery.mCustomScrollbar.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/metisMenu/metisMenu.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/metisMenu/metisMenu-vertical.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/calendar/fullcalendar.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/calendar/fullcalendar.print.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/style.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/control/css/responsive.css">
  <script src="<?= base_url() ?>assets/control/js/vendor/jquery-1.12.4.min.js"></script>
  <script src="<?= base_url() ?>assets/control/js/vendor/modernizr-2.8.3.min.js"></script>

  <script>
    $(function() {
      $("a[rel='tab']").click(function(e) {
        $(this).css({
          cursor: "wait"
        });
        pageurl = $(this).attr('href');
        $.ajax({
          url: pageurl,
          data: {
            "pageurl": pageurl
          },
          success: function(data) {
            $('#content').html();
            $('#content').html(data);
            $(this).css({
              cursor: "show"
            });
          }
        });
        if (pageurl != window.location) {
          window.history.pushState({
            path: pageurl
          }, '', pageurl);
        }
        return false;
      });
    });
  </script>
</head>

<body id="content">