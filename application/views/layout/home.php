 <div class="container-fluid">
     <div class="row">
         <div class="col-md-12">
             <h2 class="text-center">Real Time CRUD Codeigniter</h2>
             <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#ModalAdd">Add New Product</button>
             <table id="mytable" class="table table-striped">
                 <thead>
                     <tr>
                         <th>No</th>
                         <th>Product Name</th>
                         <th>Price</th>
                         <th>Action</th>
                     </tr>
                 </thead>
                 <tbody class="show_product">

                 </tbody>
             </table>
         </div>
     </div>
 </div>
 </div>

 <!-- Modal Add New Product -->
 <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="form-group">
                     <label for="input1">Product Name</label>
                     <input type="text" name="product_name" class="form-control name" id="input1" placeholder="Product Name">
                 </div>
                 <div class="form-group">
                     <label for="input2">Product Price</label>
                     <input type="text" name="product_price" class="form-control price" id="input2" placeholder="Product Price">
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-save">Save</button>
             </div>
         </div>
     </div>
 </div>

 <!-- Modal Edit Product -->
 <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="form-group">
                     <label for="input1">Product Name</label>
                     <input type="text" name="product_name" class="form-control name_edit" id="input1" placeholder="Product Name">
                 </div>
                 <div class="form-group">
                     <label for="input2">Product Price</label>
                     <input type="text" name="product_price" class="form-control price_edit" id="input2" placeholder="Product Price">
                 </div>
             </div>
             <div class="modal-footer">
                 <input type="hidden" name="product_id" class="id_edit">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary btn-edit">Edit</button>
             </div>
         </div>
     </div>
 </div>

 <!-- Modal Delete Product -->
 <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="alert alert-info">
                     Anda yakin mau menghapus data ini?
                 </div>
             </div>
             <div class="modal-footer">
                 <input type="hidden" name="product_id" class="product_id">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                 <button type="button" class="btn btn-primary btn-delete">Yes</button>
             </div>
         </div>
     </div>
 </div>
 </div>