<!-- bootstrap JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/bootstrap.min.js"></script>
<!-- wow JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/wow.min.js"></script>
<!-- price-slider JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/jquery-price-slider.js"></script>
<!-- meanmenu JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/jquery.meanmenu.js"></script>
<!-- owl.carousel JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/owl.carousel.min.js"></script>
<!-- sticky JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/jquery.sticky.js"></script>
<!-- scrollUp JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/jquery.scrollUp.min.js"></script>
<!-- counterup JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/counterup/jquery.counterup.min.js"></script>
<script src="<?= base_url() ?>assets/control/js/counterup/waypoints.min.js"></script>
<script src="<?= base_url() ?>assets/control/js/counterup/counterup-active.js"></script>
<!-- mCustomScrollbar JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?= base_url() ?>assets/control/js/scrollbar/mCustomScrollbar-active.js"></script>
<!-- metisMenu JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/metisMenu/metisMenu.min.js"></script>
<script src="<?= base_url() ?>assets/control/js/metisMenu/metisMenu-active.js"></script>


<!-- plugins JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/plugins.js"></script>
<!-- main JS
		============================================ -->
<script src="<?= base_url() ?>assets/control/js/main.js"></script>


<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script>
	$(document).ready(function() {
		// CALL FUNCTION SHOW PRODUCT
		show_product();

		// Enable pusher logging - don't include this in production
		Pusher.logToConsole = true;

		var pusher = new Pusher('0a11652d337b00b32aba', {
			cluster: 'ap1',
			forceTLS: true
		});

		var channel = pusher.subscribe('my-channel');
		channel.bind('my-event', function(data) {
			if (data.message === 'success') {
				show_product();
			}
		});

		// FUNCTION SHOW PRODUCT
		function show_product() {
			$.ajax({
				url: '<?= base_url("Home/get_product"); ?>',
				type: 'GET',
				async: true,
				dataType: 'json',
				success: function(data) {
					var html = '';
					var count = 1;
					var i;
					for (i = 0; i < data.length; i++) {
						html += '<tr>' +
							'<td>' + count++ + '</td>' +
							'<td>' + data[i].product_name + '</td>' +
							'<td>' + data[i].product_price + '</td>' +
							'<td>' +
							'<a href="javascript:void(0);" class="btn btn-sm btn-info item_edit" data-id="' + data[i].product_id + '" data-name="' + data[i].product_name + '" data-price="' + data[i].product_price + '">Edit</a>' +
							'<a href="javascript:void(0);" class="btn btn-sm btn-danger item_delete" data-id="' + data[i].product_id + '">Delete</a>' +
							'</td>' +
							'</tr>';
					}
					$('.show_product').html(html);
				}

			});
		}

		// CREATE NEW PRODUCT
		$('.btn-save').on('click', function() {
			var product_name = $('.name').val();
			var product_price = $('.price').val();
			$.ajax({
				url: '<?= base_url("Home/create"); ?>',
				method: 'POST',
				data: {
					product_name: product_name,
					product_price: product_price
				},
				success: function() {
					$('#ModalAdd').modal('hide');
					$('.name').val("");
					$('.price').val("");
				}
			});
		});
		// END CREATE PRODUCT

		// UPDATE PRODUCT
		$('#mytable').on('click', '.item_edit', function() {
			var product_id = $(this).data('id');
			var product_name = $(this).data('name');
			var product_price = $(this).data('price');
			$('#ModalEdit').modal('show');
			$('.id_edit').val(product_id);
			$('.name_edit').val(product_name);
			$('.price_edit').val(product_price);
		});

		$('.btn-edit').on('click', function() {
			var product_id = $('.id_edit').val();
			var product_name = $('.name_edit').val();
			var product_price = $('.price_edit').val();
			$.ajax({
				url: '<?= base_url("Home/update"); ?>',
				method: 'POST',
				data: {
					product_id: product_id,
					product_name: product_name,
					product_price: product_price
				},
				success: function() {
					$('#ModalEdit').modal('hide');
					$('.id_edit').val("");
					$('.name_edit').val("");
					$('.price_edit').val("");
				}
			});
		});
		// END EDIT PRODUCT

		// DELETE PRODUCT
		$('#mytable').on('click', '.item_delete', function() {
			var product_id = $(this).data('id');
			$('#ModalDelete').modal('show');
			$('.product_id').val(product_id);
		});

		$('.btn-delete').on('click', function() {
			var product_id = $('.product_id').val();
			$.ajax({
				url: '<?= base_url("Home/delete"); ?>',
				method: 'POST',
				data: {
					product_id: product_id
				},
				success: function() {
					$('#ModalDelete').modal('hide');
					$('.product_id').val("");
				}
			});
		});
		// END DELETE PRODUCT

	});
</script>
<!-- tawk chat JS